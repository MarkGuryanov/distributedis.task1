import java.sql.*;

public class Main {
    private static int INSERTS = 10000;
    private static Connection con;

    public static void main(String[] args) throws SQLException {
        con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/", "postgres", null);
        con.setAutoCommit(false);

        // warm-up
        Statement stmt = con.createStatement();
        stmt.execute("TRUNCATE test_data"); con.commit();
        testOne();
        stmt.execute("TRUNCATE test_data"); con.commit();
        testTwo();
        stmt.execute("TRUNCATE test_data"); con.commit();
        testThree();

        for (int i = 0; i < 3; i++) {
            stmt.execute("TRUNCATE test_data");
            con.commit();

            double speed = 0;
            if (0 == i) speed = testOne();
            if (1 == i) speed = testTwo();
            if (2 == i) speed = testThree();
            System.out.println("Test " + (i+1) + ": " + speed + " inserts/second");
        }

        con.setAutoCommit(true);
        stmt.execute("TRUNCATE test_data");
    }

    private static double testOne() throws SQLException {
        try (Statement stmt = con.createStatement()) {
            long startTime = System.nanoTime();
            for (int i = 0; i < INSERTS; ++i) {
                stmt.executeUpdate("INSERT INTO test_data VALUES (" + i + ", 'test1', null)");
            }
            con.commit();
            long msElapsed = System.nanoTime() - startTime;

            return INSERTS/(msElapsed*Math.pow(10, -9));
        }
    }

    private static double testTwo() throws SQLException {
        try (PreparedStatement ps = con.prepareStatement("INSERT INTO test_data VALUES (?, ?, ?)")) {
            long startTime = System.nanoTime();
            for (int i = 0; i < INSERTS; ++i) {
                ps.setInt(1, i);
                ps.setString(2, "test2");
                ps.setTimestamp(3, null);
                ps.executeUpdate();
            }
            con.commit();
            long msElapsed = System.nanoTime() - startTime;

            return INSERTS/(msElapsed*Math.pow(10, -9));
        }
    }

    private  static double testThree() throws SQLException {
        try (PreparedStatement ps = con.prepareStatement("INSERT INTO test_data VALUES (?,?, ?)")) {
            long startTime = System.nanoTime();
            for (int i = 0; i < INSERTS; i++) {
                ps.setInt(1, i);
                ps.setString(2, "test2");
                ps.setTimestamp(3, null);
                ps.addBatch();
            }
            ps.executeBatch();
            con.commit();
            long msElapsed = System.nanoTime() - startTime;

            return INSERTS/(msElapsed*Math.pow(10, -9));
        }
    }
}
